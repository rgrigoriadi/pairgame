﻿using UnityEngine;
using System.Collections.Generic;

public class CameraController : MonoBehaviour {


    private CamParams camParams;
	public const float MAX_CELLS_IN_HEIGHT_ON_SCREEN = 40.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Init(Vector2 leftBottomMapCorner, float cellsInHeightOnScreen)
    {
		if (cellsInHeightOnScreen > MAX_CELLS_IN_HEIGHT_ON_SCREEN)
			cellsInHeightOnScreen = MAX_CELLS_IN_HEIGHT_ON_SCREEN;

		float screenRatio = Screen.width / (float)Screen.height;
		float cellsInWidthOnScreen = cellsInHeightOnScreen * screenRatio;

        float left = leftBottomMapCorner.x;
        float right = leftBottomMapCorner.x + cellsInWidthOnScreen;

        float bottom = leftBottomMapCorner.y;
        float top = leftBottomMapCorner.y + cellsInHeightOnScreen;

        setCurrentParams(new CamParams(left, bottom, top));
    }

    public void Move(Vector2 deltaPos)
    {
		camParams.camLeft += deltaPos.x;
		camParams.camBottom += deltaPos.y;
		camParams.camTop += deltaPos.y;

		setCurrentParams (camParams);

        alertListeners(GetCurrentParams());
    }

	public float GetCurrentCellsInHeightOnScreen()
	{
		return GetComponent<Camera> ().orthographicSize * 2.0f;
	}

    public void ChangeScale(float newCellsInHeightOnScreen)
    {
		if (newCellsInHeightOnScreen > MAX_CELLS_IN_HEIGHT_ON_SCREEN)
			newCellsInHeightOnScreen = MAX_CELLS_IN_HEIGHT_ON_SCREEN;

		float x = camParams.camLeft + (camParams.camRight() - camParams.camLeft) / 2.0f;
		float y = camParams.camBottom + (camParams.camTop - camParams.camBottom) / 2.0f;
		float z = transform.position.z;

		float screenRatio = Screen.width / (float)Screen.height;
		float newCellsInWidthOnScreen = newCellsInHeightOnScreen * screenRatio;

		float left = x - newCellsInWidthOnScreen / 2.0f;
		float right = x + newCellsInWidthOnScreen / 2.0f;
		
		float bottom = y - newCellsInHeightOnScreen / 2.0f;
		float top = y + newCellsInHeightOnScreen / 2.0f;
		
		setCurrentParams(new CamParams(left, bottom, top));
        alertListeners(GetCurrentParams());
    }

	private void setCurrentParams (CamParams newParams)
	{
		this.camParams = newParams;

		float x = camParams.camLeft + (camParams.camRight() - camParams.camLeft) / 2.0f;
		float y = camParams.camBottom + (camParams.camTop - camParams.camBottom) / 2.0f;
		float z = transform.position.z;
		
		transform.localPosition = new Vector3(x, y, z);
		GetComponent<Camera> ().orthographicSize = (camParams.camTop - camParams.camBottom) / 2.0f;

	}

    public CamParams GetCurrentParams()
    {
        return camParams;
    }

    // Listener pattern realization
    private HashSet<ICameraChangeListener> listeners = new HashSet<ICameraChangeListener>();

    private void alertListeners(CamParams p)
    {
        foreach (ICameraChangeListener listener in listeners)
            listener.OnCamChange(p);
    }

    public void StartListening(ICameraChangeListener listener)
    {
        listeners.Add(listener);
    }

    public void StopListening(ICameraChangeListener listener)
    {
        listeners.Remove(listener);
    }
}
