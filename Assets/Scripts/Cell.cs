﻿using UnityEngine;
using System.Collections;

public class Cell {

    private int playerId;

    public void SetPlayer(int playerId)
    {
		this.playerId = playerId;
    }

    public int GetPlayer()
    {
        return playerId;
    }
}
