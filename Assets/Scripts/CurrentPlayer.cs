﻿using UnityEngine;
using System.Collections.Generic;

public class CurrentPlayer {

	public readonly int Id;
	public readonly Color Color;

	public const int NOT_FRIENDS = 0;
	public const int I_SENT_REQUEST = 1;
	public const int I_RECEIVED_REQUEST = 2;
	public const int WE_ARE_FRIENDS = 3;


	public Dictionary<int, int> friendshipStatuses = new Dictionary<int, int>();

	public CurrentPlayer(int id, Color color)
	{
		this.Id = id;
		this.Color = color;
	}

	public List<int> Friends(PlayersHandler ph) {
		List<int> ret = new List<int> ();
		foreach (int id in ph.GetPlayerList())
			if (friendshipStatuses.ContainsKey(id) && friendshipStatuses[id] == WE_ARE_FRIENDS)
				ret.Add (id);
		return ret;
	}

	public List<int> RequestSent(PlayersHandler ph) {
		List<int> ret = new List<int> ();
		foreach (int id in ph.GetPlayerList())
			if (friendshipStatuses.ContainsKey(id) && friendshipStatuses[id] == I_SENT_REQUEST)
				ret.Add (id);
		return ret;
	}

	public List<int> RequestReceived(PlayersHandler ph) {
		List<int> ret = new List<int> ();
		foreach (int id in ph.GetPlayerList())
			if (friendshipStatuses.ContainsKey(id) && friendshipStatuses[id] == I_RECEIVED_REQUEST)
				ret.Add (id);
		return ret;
	}

	public List<int> NotFriends(PlayersHandler ph) {
		List<int> ret = new List<int> ();
		ret.AddRange (ph.GetPlayerList ());
		ret.Remove(Id);
		foreach (int id in ph.GetPlayerList())
			if (friendshipStatuses.ContainsKey(id) && (friendshipStatuses[id] == WE_ARE_FRIENDS 
			    || friendshipStatuses[id] == I_SENT_REQUEST 
			    || friendshipStatuses[id] == I_RECEIVED_REQUEST))
				ret.Remove (id);
		return ret;
	}


}
