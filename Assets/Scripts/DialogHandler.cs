﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;

public class DialogHandler : MonoBehaviour {

	public Transform CancelDialogPrefab;
	public Transform OkDialogPrefab;
	public Transform YesNoDialogPrefab;

	private GameController gameController;
	private List<DialogWindow> shownDialogs = new List<DialogWindow>();
	private int dy = 0;

	private const int MARGIN = 10;

	public void Init(GameController gameController) {
		this.gameController = gameController;
	}


	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public DialogWindow ShowCancelDialog(string titleText, UnityAction cancelPressCallback)
	{
		DialogWindow dw = ShowDialog (CancelDialogPrefab);
		dw.InitCancelDialog (this, titleText, cancelPressCallback);
		return dw;
	}

	public DialogWindow ShowOkDialog(string titleText, UnityAction okPressCallback)
	{
		DialogWindow dw = ShowDialog (OkDialogPrefab);
		dw.InitOkDialog (this, titleText, okPressCallback);
		return dw;
	}

	public DialogWindow ShowYesNoDialog(string titleText, UnityAction yesPressCallback, UnityAction noPressCallback)
	{
		DialogWindow dw = ShowDialog (YesNoDialogPrefab);
		dw.InitYesNoDialog (this, titleText, yesPressCallback, noPressCallback);
		return dw;
	}

	public DialogWindow ShowDialog(Transform prefab)
	{
		DialogWindow ret = ((Transform)GameObject.Instantiate (prefab)).GetComponent<DialogWindow> ();
		ret.transform.SetParent(GameObject.Find ("Canvas").transform, false);
		ret.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (0, 0, -1);
		shownDialogs.Add (ret);

		RedrawAllDialogsWithNewPos ();
		return ret;
	}

	public void RedrawAllDialogsWithNewPos() {
		dy = 0;

		foreach (DialogWindow dw in shownDialogs) {
			// Set position
			dw.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (
				-Screen.width / 2 + dw.GetComponent<RectTransform> ().sizeDelta.x / 2 + MARGIN, 
				Screen.height / 2 - (dy + dw.GetComponent<RectTransform> ().sizeDelta.y / 2 + MARGIN),
				-1);

			dy += (int)dw.GetComponent<RectTransform> ().sizeDelta.y + MARGIN;	
		}
	}

	public void DismissDialog(DialogWindow d)
	{
		GameObject.Destroy (d.gameObject);
		shownDialogs.Remove (d);

		RedrawAllDialogsWithNewPos ();
	}

	public void UpdateDialogs(CurrentPlayer player, PlayersHandler ph)
	{
		while (shownDialogs.Count > 0)
			DismissDialog (shownDialogs[0]);

		string stra = "Not friends = ";

		foreach (int i in player.NotFriends(ph)) 
			stra += " " + i;

		Debug.Log(stra);

		foreach (int i in player.NotFriends(ph)) {
			int id = i;
			string nick = ph.GetNicknameForID(id);
			Color color = ph.GetColorForID(id);
			ShowOkDialog("Add id " + nick + " as a friend?", () => {
				gameController.SendFriendshipRequest(id);
				Debug.Log("Adding " + nick + " as a friend.");
			}).GetComponent<Image>().color = color;
		}

		//Debug.LogError ("Not a friends " + player.NotFriends(ph).Count);

		foreach (int i in player.Friends(ph)) {
			int id = i;
			string nick = ph.GetNicknameForID(id);
			Color color = ph.GetColorForID(id);
			ShowCancelDialog("You are friends with " + nick  + "?", () => {
				gameController.DestroyFriendship(id);
				Debug.Log("Removing " + nick + " from friends.");
			}).GetComponent<Image>().color = color;
		}

		foreach (int i in player.RequestSent(ph)) {
			int id = i;
			string nick = ph.GetNicknameForID(id);
			Color color = ph.GetColorForID(id);
			ShowCancelDialog("Sent " + nick + " friend request.", () => {
				gameController.CancelFriendshipRequest(id);
				Debug.Log("Canceling " + nick + " friend request.");
			}).GetComponent<Image>().color = color;
		}

		foreach (int i in player.RequestReceived(ph)) {
			int id = i;
			string nick = ph.GetNicknameForID(id);
			Color color = ph.GetColorForID(id);
			ShowYesNoDialog("Player " + id + " want you!", () => {
				gameController.AcceptFriendshipRequest(id);
				Debug.Log("Accepting " + nick + " friend request.");
			}, () => {
				gameController.DenyFriendshipRequest(i);
				Debug.Log("Refusing " + nick + " friend request.");
			}).GetComponent<Image>().color = color;
		}
	}
}




