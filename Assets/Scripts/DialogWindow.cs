﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;

public class DialogWindow : MonoBehaviour {
	public DialogHandler handler;
	public void Init(DialogHandler handler)
	{
		this.handler = handler;
	}
	
	public static Transform GetChildWithName(Transform parent, string name)
	{
		for (int i = 0; i < parent.childCount; i++)
			if (parent.GetChild (i).gameObject.name == name)
				return parent.GetChild (i);
		return null;
	}

	public void InitCancelDialog(DialogHandler handler, string titleText, UnityAction cancelPressCallback)
	{
		Init (handler);
		GetChildWithName (transform, "TitleText").GetComponent<Text> ().text = titleText;
		GetChildWithName (transform, "CancelButton").GetComponent<Button> ().onClick.AddListener (
			()=>{
			cancelPressCallback();
			Dismiss();
		});
	}

	public void InitOkDialog(DialogHandler handler, string titleText, UnityAction okPressCallback)
	{
		Init (handler);
		GetChildWithName (transform, "TitleText").GetComponent<Text> ().text = titleText;
		GetChildWithName (transform, "OkButton").GetComponent<Button> ().onClick.AddListener (
			()=>{
			okPressCallback();
			Dismiss();
		});
	}

	public void InitYesNoDialog(DialogHandler handler, string titleText, UnityAction yesPressCallback, UnityAction noPressCallback)
	{
		Init (handler);
		GetChildWithName (transform, "TitleText").GetComponent<Text> ().text = titleText;
		GetChildWithName (transform, "YesButton").GetComponent<Button> ().onClick.AddListener (() => {
			yesPressCallback();
			Dismiss();
		});
		GetChildWithName (transform, "NoButton").GetComponent<Button> ().onClick.AddListener (() => {
			noPressCallback();
			Dismiss();
		});
	}

	public void Dismiss() 
	{
		handler.DismissDialog (this);
	}

}
