﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON; 
using System.Collections;

public class GameController : MonoBehaviour {

    private Map map;
	private PlayersHandler players;
	private CurrentPlayer currentPlayer;

	private CameraController cam;
	private InputController input;
	private MapRepresentation mapRepr;
	public Map Map { get { return map; } }
	public MapRepresentation MapRepr { get { return mapRepr; } }
	public InputController InputCont { get { return input; } } 


	public PlayersHandler PlayersHandler { get { return players; } }

	public List<string> inputMessagesBuffer = new List<string>();
	public List<string> outputMessagesBuffer = new List<string>();

	private InitializationProcess initializationController;
	private bool initializationFinished = false;
	public void InitFinished() { initializationFinished = true; }

	IEnumerator testCoroutine()
	{
		for (int i = 0; i < 10000; i++)
		{
			Debug.Log (i);
			yield return null;
		}
		
	}

	// Use this for initialization
	void Start () {
	
//		StartCoroutine(testCoroutine());
		
		
	
	
		initializationController = GameObject.FindObjectOfType<InitializationProcess>();
		initializationController.RequestServerInfoFromClient(this, FindObjectOfType<ServerConnectivity>());
	
//		FindObjectOfType<ServerConnectivity> ().Run (this, initializationController);
		FindObjectOfType<DialogHandler>().Init (this);

		players = new PlayersHandler();

		cam = FindObjectOfType<CameraController> ();
		
		input = FindObjectOfType<InputController> ();
		
		mapRepr = FindObjectOfType<MapRepresentation> ();

	}

	void OnApplicationQuit() {
		Debug.Log("Gonna stop server by app quit.");
		FindObjectOfType<ServerConnectivity>().Stop();
	}

	private bool thisPlayerCame = false;
	private bool mapInfoCame = false;
	private bool playersInfoCame = false;

	private void processMessage(string str) {
		//		Debug.Log ("Processing message: '" + str + "'");
		JSONNode topNode = JSONNode.Parse (str);
		
		//		Debug.Log ("JSON Parsed:");
		//		Debug.Log (node.ToString ());

//		return;
		
		if (topNode ["messageName"].Value == "ok")
			initializationController.ServerConnectionResult(true);
		else if (topNode ["messageName"].Value == "initNickname")
		{
			string answer = topNode["data"][0].Value;
			initializationController.NicknameSendResult(answer == "ok", answer);
			if (answer == "ok")
			{
				float[,] arr = new float[3, 4];
				for (int i = 0; i < 3; i++)
					for (int j = 0; j < 4; j++)
						arr[i, j] = topNode["data"][1][i][j].AsFloat;
				initializationController.ShowColorPickDialog(
					new Color(arr[0, 0], arr[0, 1], arr[0, 2], arr[0, 3]), 
					new Color(arr[1, 0], arr[1, 1], arr[1, 2], arr[1, 3]), 
					new Color(arr[2, 0], arr[2, 1], arr[2, 2], arr[2, 3]));
			}
		} else if (topNode ["messageName"].Value == "initColor")
		{
			string answer = topNode["data"][0].Value;
			
			Color[] colors = new Color[3];
			if (answer != "ok")
			{
				float[,] arr = new float[3, 4];
		        for (int i = 0; i < 3; i++)
		        for (int j = 0; j < 4; j++)
						arr[i, j] = topNode["data"][1][i][j].AsFloat;
		        
				colors[0] = new Color(arr[0, 0], arr[0, 1], arr[0, 2], arr[0, 3]); 
				colors[1] = new Color(arr[1, 0], arr[1, 1], arr[1, 2], arr[1, 3]); 
				colors[2] = new Color(arr[2, 0], arr[2, 1], arr[2, 2], arr[2, 3]);
			}
			
			initializationController.ColorSendResult(answer == "ok", answer, colors);
		} else if (topNode ["messageName"].Value == "initTerritory")
		{
			string answer = topNode["data"][0].Value;
			initializationController.PositionSendResult(answer == "ok", answer);
		} else if (topNode ["messageName"].Value == "updatePacked") {
			foreach (JSONNode innerNode in topNode["data"].AsArray)
			{
				switch (innerNode ["messageName"]) {
				case "thisPlayerInfo":
					InitPlayer (innerNode ["data"] ["id"].AsInt, 
	                  new Color (innerNode ["data"] ["color"] [0].AsFloat,
					          innerNode ["data"] ["color"] [1].AsFloat,
					          innerNode ["data"] ["color"] [2].AsFloat,
					          innerNode ["data"] ["color"] [3].AsFloat));

//					List<int> friends = new List<int>();
//					foreach (JSONNode n in innerNode["data"]["friendships"].AsArray)
//						friends.Add(n.AsInt);
//
//					List<int> friendsThis2Other = new List<int>();
//					foreach (JSONNode n in innerNode["data"]["this2other_RequestFriendship"].AsArray)
//						friendsThis2Other.Add(n.AsInt);
//
//					List<int> friendsOther2This = new List<int>();
//					foreach (JSONNode n in innerNode["data"]["other2this_RequestFriendship"].AsArray)
//						friendsOther2This.Add(n.AsInt);
//
//					currentPlayer.friends = friends;
//					currentPlayer.friendRequestsToOthers = friendsThis2Other;
//					currentPlayer.friendRequestsToThis = friendsOther2This;



					Dictionary<int, int> friendships = new Dictionary<int, int>();
					foreach (JSONNode n in innerNode["data"]["friendships"].AsArray)
					{
//						Debug.LogWarning ("Node : " + n.ToString());
//						Debug.LogWarning ("Node : " + n.AsObject.Value);
//						Debug.LogWarning ("Node : " + n[0].AsObject.Value);
//						Debug.LogWarning ("Node : " + n[0].Value);
//						Debug.LogWarning ("Node : " + n[n[0].Value].Value);
						friendships.Add(n["id"].AsInt, n["relation"].AsInt);
					}

					currentPlayer.friendshipStatuses = friendships;

					Debug.LogWarning ("This player info came!");
					thisPlayerCame = true;
					if (playersInfoCame && mapInfoCame && thisPlayerCame)
					{
						mapRepr.OnMapChange ();
						if (initializationFinished)
							FindObjectOfType<DialogHandler>().UpdateDialogs(currentPlayer, players);
					}

					Debug.Log (currentPlayer.Friends(players).Count + " friends, requests = " + currentPlayer.RequestSent(players).Count + ", " + currentPlayer.RequestReceived(players).Count);
					break;
				case "mapInfo":
					if (map == null)
							InitMap (innerNode ["data"] ["width"].AsInt, innerNode ["data"] ["height"].AsInt);

					int[,] mapData = new int[map.Width, map.Height];

					for (int j = 0; j < map.Height; j++)
							for (int i = 0; i < map.Width; i++)
									mapData [i, j] = innerNode ["data"] ["value"] [j] [i].AsInt;
					Debug.LogWarning ("Map updated!");
					mapInfoCame = true;
					UpdateMap (mapData);
					if (playersInfoCame && mapInfoCame && thisPlayerCame)
					{
						mapRepr.OnMapChange ();
						if (initializationFinished)
							FindObjectOfType<DialogHandler>().UpdateDialogs(currentPlayer, players);
					}
					break;
				case "playersListInfo":
					List<PlayersHandler.Player> p = new List<PlayersHandler.Player> ();
					foreach (JSONNode n in innerNode["data"].AsArray) {
							p.Add (
								new PlayersHandler.Player (n ["id"].AsInt, 
								n ["nickname"].Value, 
								new Color (n ["color"] [0].AsFloat, n ["color"] [1].AsFloat, n ["color"] [2].AsFloat, n ["color"] [3].AsFloat)));
					}
					Debug.LogWarning ("Player list updated:");
					players.UpdatePlayerList (p);
					playersInfoCame = true;
					if (playersInfoCame && mapInfoCame && thisPlayerCame)
					{
						mapRepr.OnMapChange ();
						if (initializationFinished)
							FindObjectOfType<DialogHandler>().UpdateDialogs(currentPlayer, players);
					}

					break;
				default:
					Debug.LogError ("Unknown message: '" + str + "'");
					break;
				}
			}
		} else {
			Debug.LogError ("Unknown message: '" + str + "'");
		}
	}

//	public void SendCoords(Vector2 coord)
//	{
//		JSONNode node = new JSONClass ();
//		node["messageName"] = "testCoord";
//		node ["data"] ["x"].AsInt = (int) coord.x;
//		node ["data"] ["y"].AsInt = (int) coord.y;
//
//		SendMessage (node.ToString ());
//	}

	public void SendInitNickname(string nickname)
	{
		JSONNode node = new JSONClass();
		node["messageName"] = "initNickname";
		node["data"]["nickname"] = nickname;
		SendServerMessage(node.ToString());
	}

	public void SendInitColor(Color c)
	{
		JSONNode node = new JSONClass();
		node["messageName"] = "initColor";
		node["data"]["color"][0].AsFloat = c.r;
		node["data"]["color"][1].AsFloat = c.g;
		node["data"]["color"][2].AsFloat = c.b;
		node["data"]["color"][3].AsFloat = c.a;
		SendServerMessage(node.ToString());
	}
	
	public void SendInitPosition(int x, int y) 
	{
		JSONNode node = new JSONClass();
		node["messageName"] = "initTerritory";
		node["data"]["x"].AsInt = x;
		node["data"]["y"].AsInt = y;
		SendServerMessage(node.ToString());
	}

	public void SendFriendshipRequest (int id) { SendFriendMessage ("requestFriendship", id); }
	public void CancelFriendshipRequest (int id) { SendFriendMessage ("cancelYourFriendshipRequest", id); }
	public void AcceptFriendshipRequest (int id) { SendFriendMessage ("acceptFriendshipRequest", id); }
	public void DenyFriendshipRequest (int id) { SendFriendMessage ("denyFriendshipRequest", id); }
	public void DestroyFriendship (int id) { SendFriendMessage ("destroyFriendship", id); }

	public void SendFriendMessage(string messageName, int id)
	{
		JSONNode node = new JSONClass ();
		node ["messageName"] = messageName;
		node ["data"] ["id"].AsInt = id;
		SendServerMessage (node.ToString());
	}

	public void SendServerMessage(string str)
	{
		lock (outputMessagesBuffer) {
			outputMessagesBuffer.Add (str);
		}
	}
	
	public void InitPlayer(int playerId, Color playerColor) 
	{
		currentPlayer = new CurrentPlayer (playerId, playerColor);
	}

	public void InitMap(int w, int h)
	{
		Debug.Log ("Map inited; w, h = " + w + ", " + h);

		this.map = new Map (w, h);

		cam.Init (new Vector2 (0, 0), h);
		input.Init (FindObjectOfType<CameraController> (), this);
		mapRepr.Init (this, map, FindObjectOfType<CameraController> ());

	}

	public void UpdateMap(int[,] newMapData)
	{
		map.UpdateMap (newMapData);
	}

	// Update is called once per frame
	void Update () {
		if (inputMessagesBuffer.Count != 0)
			lock (inputMessagesBuffer) {
				foreach (string s in inputMessagesBuffer)
					processMessage(s);
				inputMessagesBuffer.Clear();
			}


	}
}
