﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;

public class InitializationProcess : MonoBehaviour, IMapClickListener {

	private string ServerIP;
	private int Port;
	
	private GameObject currentDialog;
	
	public const string SERVER_ADDRESS = "nkchehov.koding.io";//"52.0.249.160";
	public const int SERVER_PORT = 5125;
	
	public Transform OkDialogPrefab;
	public Transform ServerInfoDialogPrefab;
	public Transform PlayerNicknameDialogPrefab;
	public Transform PlayerColorDialogPrefab;
	
	GameController gc;
	ServerConnectivity sc;
	
	public void RequestServerInfoFromClient(GameController gc, ServerConnectivity serverConn)
	{
		this.gc = gc;
		this.sc = serverConn;
		
		showDialog(ServerInfoDialogPrefab);
		
			DialogWindow.GetChildWithName(currentDialog.transform, "IPInputField")
			.GetComponent<InputField>().text = "" + SERVER_ADDRESS;//"52.0.223.242";//System.Net.Dns.GetHostEntry(SERVER_ADDRESS).AddressList[0];
		
		
			DialogWindow.GetChildWithName(currentDialog.transform, "PortInputField")
			.GetComponent<InputField>().text = "" + SERVER_PORT;
		
		DialogWindow.GetChildWithName(currentDialog.transform, "OkButton").GetComponent<Button>().onClick.AddListener(() => 
		{
			string server = 
				DialogWindow.GetChildWithName(currentDialog.transform, "IPInputField").GetComponent<InputField>().text;
				//	.transform, "Text").GetComponent<Text>().text;
					
			string port = 
				DialogWindow.GetChildWithName(currentDialog.transform, "PortInputField").GetComponent<InputField>().text;
					//.transform, "Text").GetComponent<Text>().text;
			ServerInfoEntered(server, System.Convert.ToInt32(port));
			hideCurrentDialog();
		});
		
		//ShowColorPickDialog(Color.red, Color.green, Color.blue);
	}
	
	public void ServerInfoEntered(string ip, int port)
	{
		sc.Run (gc, ip, port, this);
	}
	
	public void ServerConnectionResult(bool connectedSuccesfuly) {
		if (connectedSuccesfuly)
		{
			//show Nickname dialog
			
			showDialog(PlayerNicknameDialogPrefab);
			
			DialogWindow.GetChildWithName(currentDialog.transform, "OkButton").GetComponent<Button>().onClick.AddListener(() => 
			{
				string nick = 
					DialogWindow.GetChildWithName(currentDialog.transform, "NicknameInputField").GetComponent<InputField>().text;
				//	.transform, "Text").GetComponent<Text>().text;
				Debug.Log ("Nick entered = " + nick);
				NicknameInfoEntered(nick);
				hideCurrentDialog();	
			});
		}
	}
	
	public void NicknameInfoEntered(string nickname)
	{
		gc.SendInitNickname(nickname);
	}
	
	public void NicknameSendResult(bool result, string reason)
	{
		if (result)
		{
			//Waiting for colors // show color dialog
			//ShowColorPickDialog(Color.red, Color.green, Color.blue);
		}
		else 
		{
			// show error dialog
			showErrorDialog(reason, () => {
				// close it
				hideCurrentDialog();
				// open nickName
				ServerConnectionResult(true);	
			});
		}
	}
	
	public void ShowColorPickDialog(Color a, Color b, Color c)
	{
		showDialog(PlayerColorDialogPrefab);
		
		ColorBlock colorBlock = DialogWindow.GetChildWithName(currentDialog.transform, "Color1").GetComponent<Toggle>().colors;
		
		colorBlock.normalColor = a;
		colorBlock.highlightedColor = a;
		colorBlock.pressedColor = a;
		DialogWindow.GetChildWithName(currentDialog.transform, "Color1").GetComponent<Toggle>().colors = colorBlock;
		DialogWindow.GetChildWithName(currentDialog.transform, "Color1").GetComponent<Toggle>().isOn = false;
		DialogWindow.GetChildWithName(currentDialog.transform, "Color1").GetComponent<Toggle>().onValueChanged.AddListener((bool isPressed) => 
		{
			if (isPressed)
			{
				DialogWindow.GetChildWithName(currentDialog.transform, "Color2").GetComponent<Toggle>().isOn = false;
				DialogWindow.GetChildWithName(currentDialog.transform, "Color3").GetComponent<Toggle>().isOn = false;
			}
		});	
		
		colorBlock.normalColor = b;
		colorBlock.highlightedColor = b;
		colorBlock.pressedColor = b;
		DialogWindow.GetChildWithName(currentDialog.transform, "Color2").GetComponent<Toggle>().colors = colorBlock;
		DialogWindow.GetChildWithName(currentDialog.transform, "Color2").GetComponent<Toggle>().isOn = false;
		DialogWindow.GetChildWithName(currentDialog.transform, "Color2").GetComponent<Toggle>().onValueChanged.AddListener((bool isPressed) => 
		{ 
			if (isPressed)
			{
				DialogWindow.GetChildWithName(currentDialog.transform, "Color1").GetComponent<Toggle>().isOn = false;
				DialogWindow.GetChildWithName(currentDialog.transform, "Color3").GetComponent<Toggle>().isOn = false;
			}
		});	
		
		colorBlock.normalColor = c;
		colorBlock.highlightedColor = c;
		colorBlock.pressedColor = c;
		DialogWindow.GetChildWithName(currentDialog.transform, "Color3").GetComponent<Toggle>().colors = colorBlock;
		DialogWindow.GetChildWithName(currentDialog.transform, "Color3").GetComponent<Toggle>().isOn = false;
		DialogWindow.GetChildWithName(currentDialog.transform, "Color3").GetComponent<Toggle>().onValueChanged.AddListener((bool isPressed) => 
		{
			if (isPressed)
			{
				DialogWindow.GetChildWithName(currentDialog.transform, "Color1").GetComponent<Toggle>().isOn = false;
				DialogWindow.GetChildWithName(currentDialog.transform, "Color2").GetComponent<Toggle>().isOn = false;
			}
		});	
		
		
		DialogWindow.GetChildWithName(currentDialog.transform, "OkButton").GetComponent<Button>().onClick.AddListener(() => 
		{
		    if (DialogWindow.GetChildWithName(currentDialog.transform, "Color1").GetComponent<Toggle>().isOn)
		    	gc.SendInitColor(a);
			else if (DialogWindow.GetChildWithName(currentDialog.transform, "Color2").GetComponent<Toggle>().isOn)
				gc.SendInitColor(b);
			else if (DialogWindow.GetChildWithName(currentDialog.transform, "Color3").GetComponent<Toggle>().isOn)
				gc.SendInitColor(c);
			else
			{
				Debug.LogError("Please pick a color.");
				return;
			}
				
			hideCurrentDialog();	
		});
	}
	
	public void ColorSendResult (bool ok, string reason, Color[] colors)
	{
		if (ok)
		{
//			????? Init map
		
			// show dialog
			showErrorDialog("Choose starting position", () => {
				// close it
				hideCurrentDialog();
				gc.InputCont.StartListening(this);	
			});
			
		}
		else
		{
			// show error dialog
			showErrorDialog(reason, () => {
				// close it
				hideCurrentDialog();
				// open color picker again
				ShowColorPickDialog(colors[0], colors[1], colors[2]);	
			});
		}
	}
	
	public void OnClick(int x, int y)
	{
		gc.InputCont.StopListening(this);
		gc.SendInitPosition(x, y);
	}
	
	public void PositionSendResult(bool ok, string reason)
	{
		if (ok)
		{
			Debug.LogWarning("Prepare your anus!");
			gc.InitFinished();
		}
		else 
			{
				// show error dialog
				showErrorDialog(reason, () => {
					// close it
					hideCurrentDialog();
					// show dialog
					showErrorDialog("Choose starting position", () => {
						// close it
						hideCurrentDialog();
						gc.InputCont.StartListening(this);	
					});
				});
			}
	}
	
	private void showErrorDialog (string message, UnityAction okCallback) 
	{
		showDialog(OkDialogPrefab);
		DialogWindow.GetChildWithName(currentDialog.transform, "TitleText").GetComponent<Text>().text = message;
		DialogWindow.GetChildWithName(currentDialog.transform, "OkButton").GetComponent<Button>().onClick.AddListener(okCallback);
	}
	
	private void hideCurrentDialog() {
		if (currentDialog != null)
			GameObject.Destroy(currentDialog);
		currentDialog = null;
	}
	
	private GameObject showDialog(Transform prefab)
	{
		GameObject ret = ((Transform)GameObject.Instantiate (prefab)).gameObject;
		ret.transform.SetParent(GameObject.Find ("Canvas").transform, false);
		ret.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (0, 0, -1);
		currentDialog = ret;
		
		return ret;	
	}
}
