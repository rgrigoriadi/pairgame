﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class InputController : MonoBehaviour {//, IPointerDownHandler, IPointerUpHandler {

	private GameController gameController;
	private CameraController cam;
	private const float CAM_SPEED_UNITS_PER_SECOND = 10.0f;
	private const float CAM_ZOOM_SCALE_CHANGE_SPEED_PER_SECOND = 1.1f;

	public void Init(CameraController cam, GameController gc)
	{
		this.cam = cam;
		this.gameController = gc;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float dist = CAM_SPEED_UNITS_PER_SECOND * Time.deltaTime;

		if (Input.GetKey(KeyCode.W))
			this.cam.Move(new Vector2(0.0f, dist));

		if (Input.GetKey(KeyCode.S))
			this.cam.Move(new Vector2(0.0f, -dist));

		if (Input.GetKey(KeyCode.A))
			this.cam.Move(new Vector2(-dist, 0.0f));

		if (Input.GetKey(KeyCode.D))
			this.cam.Move(new Vector2(dist, -0.0f));

		float zoomMultiplier = 1.0f + CAM_ZOOM_SCALE_CHANGE_SPEED_PER_SECOND * (Time.deltaTime);
		if (Input.GetKey (KeyCode.Plus) || Input.GetKey(KeyCode.Equals))
			this.cam.ChangeScale (this.cam.GetCurrentCellsInHeightOnScreen() / zoomMultiplier);

		if (Input.GetKey (KeyCode.Minus))
			this.cam.ChangeScale (this.cam.GetCurrentCellsInHeightOnScreen() * zoomMultiplier);

	}

	public void SayHalo() {
		Debug.Log ("Hello!");
		}

	public void PointerDown(BaseEventData data) {//PointerEventData data) {
		if (listeners.Count > 0)
		{
			PointerEventData ped = ((PointerEventData)data);
			Debug.LogWarning ("Down at " + ped.position);
	
			Vector2 pos = gameController.MapRepr.CellByScreenCoords(cam.GetCurrentParams(), ped.position);
			Debug.LogWarning ("Touched @ " + pos);
			Debug.LogWarning ("Id = " + gameController.Map.GetAtomsPlayer((int) pos.x, (int) pos.y));
	//		gameController.SendCoords (pos);
	
			alertListeners((int) pos.x, (int) pos.y);
		}
	}

	public void PointerUp(BaseEventData data) {//PointerEventData data) {
		Debug.LogWarning ("Up at " + ((PointerEventData) data).position);
	}
	
	public void OnExitClick() {
		Application.Quit();
	}
	
	private HashSet<IMapClickListener> listeners = new HashSet<IMapClickListener>();
	private HashSet<IMapClickListener> listenersToRemove = new HashSet<IMapClickListener>();
	
	private void alertListeners(int x, int y)
	{
		foreach (IMapClickListener listener in listeners)
			listener.OnClick(x, y);
		foreach (IMapClickListener listener in listenersToRemove)
			listeners.Remove(listener);
		listenersToRemove.Clear();
	}
	
	public void StartListening(IMapClickListener listener)
	{
		listeners.Add(listener);
	}
	
	public void StopListening(IMapClickListener listener)
	{
		//listeners.Remove(listener);
		listenersToRemove.Add(listener);
	}
}

public interface IMapClickListener {
	void OnClick(int x, int y);
}