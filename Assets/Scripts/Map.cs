﻿using UnityEngine;
using System.Collections.Generic;

public class Map {

    public readonly int Width;
    public readonly int Height;

    private Cell[,] cells;
    

    public Map(int width, int height)
    {
        this.Width = width;
        this.Height = height;

        cells = new Cell[width, height];
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                cells[i, j] = new Cell();
    }

	public void UpdateMap(int[,] playerIds)
	{
		for (int i = 0; i < Width; i++)
			for (int j = 0; j < Height; j++)
				SetAtomsPlayer (i, j, playerIds [i, j]);
//		alertListeners ();
	}

    private void SetAtomsPlayer(int x, int y, int playerId)
    {
        if (x >= 0 && x < Width && y >= 0 && y < Height)
			cells[x, y].SetPlayer(playerId);
        else
            Debug.LogError("Illegal index! x = " + x + " y = " + y + " | size = " + Width + ", " + Height);
    }

    public int GetAtomsPlayer(int x, int y)
    {
        if (x >= 0 && x < Width && y >= 0 && y < Height)
            return cells[x, y].GetPlayer();
        else
			Debug.LogError("Illegal index! x = " + x + " y = " + y + " | size = " + Width + ", " + Height);
        return -1;
    }

	public class Pair{
		public int a;
		public int b;
		public Pair(int a, int b)
		{
			this.a = a;
			this.b = b;
		}
	}

	public void UpdateMapByAlgorithm(List<Pair> friendShips)
	{
		int[,] newMap = new int[Width, Height];
		for (int i = 0; i < Width; i++)
			for (int j = 0; j < Height; j++)
				newMap[i, j] = -1;
				
		for (int i = 0; i < Width; i++)
			for (int j = 0; j < Height; j++)
			{
				int nearestInterestX = 0;
				int nearestInterestY = 0;

				bool found = false;
				float dist = -1;
				
				for (int ii = 0; ii < Width; ii++)
					for (int jj = 0; jj < Height; jj++)		
					{
						float newDist = Mathf.Sqrt((i - ii) * (i - ii) + (j - jj) * (j - jj));

						if (friends(GetAtomsPlayer(i, j), GetAtomsPlayer(ii, jj), friendShips))
							if (newDist < dist || dist == -1)
							{
								dist = newDist;
								nearestInterestX = ii;
								nearestInterestY = jj;
								found = true;
							}
					}
				
				if (GetAtomsPlayer(i, j) != -1)
					newMap[i, j] = GetAtomsPlayer(i, j);
							
				if (found)
				{
					int dx = (nearestInterestX - i);	dx = dx > 0 ? 1 : dx == 0 ? 0 : -1;
					int dy = (nearestInterestY - j);	dy = dy > 0 ? 1 : dy == 0 ? 0 : -1;
				
					int nextCellX = i + dx;
					int nextCellY = j + dy;
					
					newMap[nextCellX, nextCellY] = GetAtomsPlayer(i, j);
				}
			}
		UpdateMap(newMap);
	}

	public bool friends(int a, int b, List<Map.Pair> friendships)
	{
		foreach (Map.Pair p in friendships)
			if ((a == p.a && b == p.b) || (a == p.b && b == p.a))
				return true;
		return false;
	}

	public int[,] GetCurrentState() {
		int[,] ret = new int[Width, Height];
		for (int i = 0; i < Width; i++)
			for (int j = 0; j < Height; j++)
				ret[i, j] = GetAtomsPlayer(i, j);

		return ret;
	}

	public static List<Vector2> LineAlgorithm(Vector2 a, Vector2 b)
	{
		List<Vector2> ret = new List<Vector2>();
		
		float dx = b.x - a.x;
		float dy = b.y - a.y;
		
		if (Mathf.Abs(dx) >= Mathf.Abs(dy))
			for (int i = 1; i < Mathf.Abs(dx); i++)
			{
				float x = a.x + Mathf.Sign(dx) * i;
				float yIn = a.y + Mathf.Sign(dy) * (i - 0.5f) * Mathf.Abs(dy / dx);
				float yOut = a.y + Mathf.Sign(dy) * (i + 0.5f) * Mathf.Abs(dy / dx);
				if ((Mathf.Ceil(yIn) == Mathf.Ceil(yOut) && Mathf.Floor(yIn) == Mathf.Floor(yOut)) 
					|| ((yIn - Mathf.Floor(yIn) == 0) && (yOut - Mathf.Floor(yOut) == 0)))
				{
					float y = Mathf.Floor(Mathf.Min (yIn, yOut)/*(yIn + yOut) / 2.0f*/) + 0.5f;
					ret.Add(new Vector2(x, y));	
				}
				else 
				{
					float y1 = Mathf.Floor(yIn) + 0.5f;	
					float y2 = Mathf.Floor(yOut) + 0.5f;
					ret.Add(new Vector2(x, y1));	
					ret.Add(new Vector2(x, y2));	
				}
			}
		else
			for (int i = 1; i < Mathf.Abs(dy); i++)
			{
				float y = a.y + Mathf.Sign(dy) * i;
				float xIn = a.x + Mathf.Sign(dx) * (i - 0.5f) * Mathf.Abs(dx / dy);
				float xOut = a.x + Mathf.Sign(dx) * (i + 0.5f) * Mathf.Abs(dx / dy);
				if ((Mathf.Ceil(xIn) == Mathf.Ceil(xOut) && Mathf.Floor(xIn) == Mathf.Floor(xOut)) 
				    || ((xIn - Mathf.Floor(xIn) == 0) && (xOut - Mathf.Floor(xOut) == 0)))
				{
					float x = Mathf.Floor(Mathf.Min (xIn, xOut)/*(xIn + xOut) / 2.0f*/) + 0.5f;
					ret.Add(new Vector2(x, y));	
				}
				else 
				{
					float x1 = Mathf.Floor(xIn) + 0.5f;	
					float x2 = Mathf.Floor(xOut) + 0.5f;
					ret.Add(new Vector2(x1, y));	
					ret.Add(new Vector2(x2, y));	
				}
			}			
		return ret;
	}


    // Listener pattern realization
    private HashSet<IMapChangeListener> listeners = new HashSet<IMapChangeListener>();

    private void alertListeners()
    {
        foreach (IMapChangeListener listener in listeners)
            listener.OnMapChange();
    }

    public void StartListening(IMapChangeListener listener)
    {
        listeners.Add(listener);
    }

    public void StopListening(IMapChangeListener listener)
    {
        listeners.Remove(listener);
    }
}
