﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;


public class MapRepresentation : MonoBehaviour, IMapChangeListener, ICameraChangeListener {

    private CellRepresentation[] cellsDrawn;
    private Map map;
    private CameraController cam;
	GameController gameController;

    public Transform CellPrefab;
	public Transform PlayerPanelPrefab;

    public void Init(GameController gc, Map map, CameraController cam)
    {
        this.map = map;
        this.cam = cam;
		this.gameController = gc;

        map.StartListening(this);
        cam.StartListening(this);

		OnMapChange ();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void Undraw()
    {
        if (cellsDrawn != null)
        {
            foreach (CellRepresentation cell in cellsDrawn)
                GameObject.Destroy(cell.gameObject);
            cellsDrawn = null;
        }
    }

    void Draw(CamParams camPos)
    {
//		Debug.Log ("Drawing map: " + camPos.camLeft + ", " + camPos.camRight() + ", " + camPos.camBottom + ", " + camPos.camTop);

		Vector4[] cellsToDraw = TestDraw (camPos, map);//TestDrawWithMovingMap(camPos, TestDraw(camPos, map));
        cellsDrawn = new CellRepresentation[cellsToDraw.Length];

//		foreach (Vector4 v in cellsToDraw)
//			Debug.Log (v);


//		Debug.Log ("Drawn cells length: " + cellsToDraw.Length);

        for (int i = 0; i < cellsToDraw.Length; i++)
        {
			Vector3 cellPosition = new Vector3(cellsToDraw[i].x, cellsToDraw[i].y, 0) 
				+ new Vector3(0.5f, 0.5f, 0.0f);
			// As quad center is shifted 0.5f relative to bottom left corner

            cellsDrawn[i] = ((Transform) GameObject.Instantiate(CellPrefab)).gameObject.GetComponent<CellRepresentation>();
			Color c = gameController.PlayersHandler.GetColorForID(map.GetAtomsPlayer((int) cellsToDraw[i].z, (int) cellsToDraw[i].w));
			//Debug.Log ("Color = " + c + " for id " + map.GetAtomsPlayer((int) cellsToDraw[i].z, (int) cellsToDraw[i].w));
//			Color c = new Color(cellsToDraw[i].z / map.Width, cellsToDraw[i].w / map.Height, 0.0f);
			cellsDrawn[i].SetColor(c);
            cellsDrawn[i].gameObject.transform.position = cellPosition;

        }

		// Draw player panels
    }

    void Redraw(CamParams camPos)
    {
        Undraw();
        Draw(camPos);
    }

    public void OnMapChange()
    {
	    if (cam == null || cam.GetCurrentParams() == null)
	    {
	        Debug.LogError("Trying to redraw map without cam!");
	        return;
	    }
		
		Redraw(cam.GetCurrentParams());
    }

    public void OnCamChange(CamParams camPos)
    {
        Redraw(camPos);
    }

    public static Vector4[] TestDraw(CamParams camPos, Map map)
        // funciton created to debug results of drawing
        // it returns array of coordinates and cells to draw

        // camera is not moved, map is moved

        // returns array of cells to draw in form^
        // real position of cell in world
        // and 
    {
        int left =   Mathf.FloorToInt(camPos.camLeft);
        int right = Mathf.CeilToInt(camPos.camRight());

        int bottom = Mathf.FloorToInt(camPos.camBottom);
        int top = Mathf.CeilToInt(camPos.camTop);

        Vector4[] ret = new Vector4[(right - left) * (top - bottom)];
        // new Vector4(0, 0, 0, 0) = xreal, yreal, xOnMap, yOnMap

        for (int j = 0; j < top - bottom; j++)
            for (int i = 0; i < right - left; i++)
            {
                float realx = (left + i);
                float realy = (bottom + j);

                int xOnMap = NegativeModulo(left + i, map.Width);
                int yOnMap = NegativeModulo(bottom + j, map.Height);
                ret[j * (right - left) + i] = new Vector4(realx, realy, xOnMap, yOnMap);//i % map.Width, j % map.Height);

				if (xOnMap < 0 || xOnMap >= map.Width || yOnMap < 0 || yOnMap >= map.Height)
				Debug.Log("Error vector i j = " + i + ", " + j + " l r b t = " + left + ", " + right + ", " + bottom + ", " + top
				          + " rx ry = " + realx + ", " + realy + ", " + " xm ym = " + xOnMap + ", " + yOnMap);
            }

        return ret;
    }

	public Vector2 CellByScreenCoords(CamParams cam, Vector2 screenCoord)
	{
		Vector2 leftBottomWorld = new Vector2 (cam.camLeft, cam.camBottom);
		Vector2 dPos = new Vector2 ((screenCoord.x / (float)Screen.width) * (cam.camRight() - cam.camLeft),
		                            (screenCoord.y / (float)Screen.height) * (cam.camTop - cam.camBottom));

		Vector2 worldCoords = leftBottomWorld + dPos;

		worldCoords.x = NegativeModulo(Mathf.FloorToInt (worldCoords.x), map.Width);
		worldCoords.y = NegativeModulo(Mathf.FloorToInt (worldCoords.y), map.Height);
		return worldCoords;
	}

	public Vector2 ScreenCoordsByCellCoords(CamParams cam, Vector2 cellCoord)
	{
		Vector2 shiftedCellCoord = cellCoord - new Vector2(cam.camLeft, cam.camBottom);
		// Leftest bottomest of cyclic cells

		shiftedCellCoord = new Vector2(Mathf.Floor(shiftedCellCoord.x), Mathf.Floor(shiftedCellCoord.y));

		Vector2 shiftedModuloedCoord = new Vector2(NegativeModulo((int)shiftedCellCoord.x, map.Width), NegativeModulo((int)shiftedCellCoord.y, map.Height));

		Vector2 screenCoords = cam.WorldToScreen(shiftedModuloedCoord);

		return screenCoords;
	}

//    public static Vector4[] TestDrawWithMovingMap(CamParams camPos, Vector4[] cellsToDraw)
//    // Gets result of TestDraw function and returns
//    // that array moved and scaled, as camera is not moved,
//    // map in world is moved
//    {
//        Vector4[] ret = new Vector4[cellsToDraw.Length];
//        for (int i = 0; i < ret.Length; i++)
//        {
//            float realx = cellsToDraw[i].x;
//            float realy = cellsToDraw[i].y;
//            int xOnMap = (int) cellsToDraw[i].z;
//            int yOnMap = (int) cellsToDraw[i].w;
//
//            realx = realx - camPos.camLeft;
//            realy = realy - camPos.camBottom;
//
//            realx = realx * camPos.cellSizeInUnits;
//            realy = realy * camPos.cellSizeInUnits;
//
//            ret[i] = new Vector4(realx, realy, xOnMap,yOnMap);
//        }
//        return ret;
//    }

	public List<PlayerPanel> panelsOnScreen = new List<PlayerPanel>();

	public PlayerPanel ShowPanel(int playerId, Vector2 pos)
	{
		PlayerPanel panel = ((Transform) GameObject.Instantiate(PlayerPanelPrefab)).GetComponent<PlayerPanel>();
		panel.GetComponent<RectTransform>().SetParent(GameObject.Find("Canvas").transform);
		panel.GetComponent<RectTransform>().position = new Vector3(pos.x, pos.y, -1);

		return null;
	}

	public void HidePanel(PlayerPanel panel)
	{
		panelsOnScreen.Remove(panel);
		GameObject.Destroy(panel.gameObject);
	}

    public static int NegativeModulo(int a, int b)
    {
        if (a >= 0)
				return a % Mathf.Abs (b);
		else {
			int ret = (a % Mathf.Abs(b));
			if (ret == 0)
				return ret;
			else
				return ret + Mathf.Abs(b);
		}   
    }
}

public interface IMapChangeListener
{
    void OnMapChange();
}

public interface ICameraChangeListener
{
    void OnCamChange(CamParams camPos);
}



public class CamParams
{
    public float camLeft;
	public float camRight() {
		return camLeft + (camTop - camBottom) * (Screen.width / (float)Screen.height); 
	}
    public float camBottom;
    public float camTop;

	public CamParams(float camLeft, float camBottom, float camTop)
    {
        this.camLeft = camLeft;
        this.camBottom = camBottom;
        this.camTop = camTop;
    }

	public Vector2 WorldToScreen(Vector2 worldCoords) 
	{
		return new Vector2 (
			Screen.width * (worldCoords.x - camLeft) / (camRight() - camLeft),
			Screen.height * (worldCoords.y - camBottom) / (camTop - camBottom));
	}
}
