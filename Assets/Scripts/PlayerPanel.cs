﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerPanel : MonoBehaviour {

	private int id;

	public void InitFor(int id, PlayersHandler ph, CurrentPlayer player)
	{
		this.id = id;
		DialogWindow.GetChildWithName(transform, "Nickname").GetComponent<Text>().text = ph.GetNicknameForID(id);

	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
