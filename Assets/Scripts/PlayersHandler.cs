﻿	using UnityEngine;
using System.Collections.Generic;

public class PlayersHandler {

	List<Player> players = new List<Player>();

	public PlayersHandler() {

	}

	public string GetNicknameForID(int id) {
		foreach (Player p in players)
			if (p.Id == id)
				return p.NickName;
		return "Error";
	}
	
	public Color GetColorForID(int id) {
		foreach (Player p in players)
			if (p.Id == id)
					return p.Color;
		return new Color (0.8f, 0.8f, 0.8f);
	}

	public void UpdatePlayerList(List<Player> p)
	{
		players.Clear ();
		players.AddRange (p);

		foreach (Player pp in players)
			Debug.Log ("Player id = " + pp.Id + " Nick = '" + pp.NickName + "' color = " + pp.Color);
	}

	public class Player {
		public readonly int Id;
		public readonly Color Color;
		public readonly string NickName;

		public Player(int id, string nick, Color col)
		{
			this.Id = id;
			this.NickName = nick;
			this.Color = col;
		}
	}

	public List<int> GetPlayerList() {
		List<int> ret = new List<int>();

		foreach (Player p in players)
			ret.Add (p.Id);
		return ret;
	}
}
