﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using SimpleJSON;

public class ServerConnectivity : MonoBehaviour {

	private bool isConnectionAlive = false;
	private Cosocket coSocket;
	
	public const int MAX_BUFFER_SIZE = 10000;
	private GameController gameController;

//	private System.Threading.Thread networkInputThread = null;
//	private System.Threading.Thread networkOutputThread = null;

//	Socket socket2;
//	Socket socket3;
	
	private InitializationProcess initForInputSocket;

	public void Run(GameController gc, string ip, int port, InitializationProcess init)
	{
		this.gameController = gc;
		this.initForInputSocket = init;

		
		print("Trying connect to " + Dns.GetHostEntry(ip).AddressList[0] +":"+ port);
		initSocket(ip, port);
	}

	public void Stop() {
		isConnectionAlive = false;
		if (coSocket != null && coSocket.IsConnected())
		{
			coSocket.Shutdown(SocketShutdown.Both);
			coSocket.Close();
		}
	}

	private /*IEnumerator*/ void initSocket(string ip, int port)
	{
		isConnectionAlive = true;
		coSocket = new Cosocket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		StartCoroutine(coSocket.Connect(Dns.GetHostEntry(ip).AddressList[0], port, cosocketCallback));
		Debug.Log ("Connected");
//		yield return StartCoroutine(processAsyncBothSocketConnections());

		System.Threading.Thread.Sleep(3000);
		// only when connection established
		StartCoroutine(processAsyncInputSocketConnection());
		StartCoroutine(processAsyncOutputSocketConnection());

//		yield return null;
	}

	
	private IEnumerator processAsyncBothSocketConnections() {
		//		StartCoroutine(processAsyncOutputSocketConnection());
		
		Debug.Log ("Both streams started...");
		
		while (isConnectionAlive)
		{
			{		
				byte[] buf2 = new byte[MAX_BUFFER_SIZE];
				yield return StartCoroutine(coSocket.Receive(cosocketCallback, buf2));
				Debug.Log ("Received (" + buf2.Length + ") = " + System.Text.Encoding.UTF8.GetString(buf2));
				
				string str = System.Text.Encoding.UTF8.GetString(buf2);
				
				Debug.Log ("Socket received byte array of len = " + buf2.Length + " str.len = " + str.Length + " str = '" + str + "'");
				
				char[] separators = {'|'};
				string[] messages = str.Split(separators);
				
				lock (gameController.inputMessagesBuffer)
				{
					for (int i = 0; i < messages.Length; i++)
					{
						Debug.Log ("Splitted message " + i + " = '" + messages[i] + "'");
						if (messages[i] != "" && messages[i][0] == '{')
							gameController.inputMessagesBuffer.Add (messages[i]);
					}
				}
			}
			
			//===================
			
			{
				List<string> messagesToSend = new List<string>();
				
				if (gameController.outputMessagesBuffer.Count != 0)
				lock (gameController.outputMessagesBuffer) {
					messagesToSend.AddRange(gameController.outputMessagesBuffer);
					gameController.outputMessagesBuffer.Clear();
				}
				
				foreach (string str in messagesToSend)
				{
					Debug.Log ("Sending message: " + str);
					
					byte[] buf = System.Text.Encoding.UTF8.GetBytes(str);
					//socket.Send(buf);
					yield return StartCoroutine(coSocket.Send(cosocketCallback, buf));
				}
			}
			
		}
		
		Debug.Log ("Both streams closed...");
		
	}

	private IEnumerator processAsyncInputSocketConnection() {
		
		Debug.Log ("Input stream started...");
		
		while (isConnectionAlive)
		{		
			byte[] buf2 = new byte[MAX_BUFFER_SIZE];
			
			yield return StartCoroutine(coSocket.Receive(cosocketCallback, buf2));
			
			Debug.Log ("Received (" + buf2.Length + ") = " + System.Text.Encoding.UTF8.GetString(buf2));
			
			string str = System.Text.Encoding.UTF8.GetString(buf2);

			Debug.Log ("Socket received byte array of len = " + buf2.Length + " str.len = " + str.Length + " str = '" + str + "'");
	
			char[] separators = {'|'};
			string[] messages = str.Split(separators);
			
			lock (gameController.inputMessagesBuffer)
			{
				for (int i = 0; i < messages.Length; i++)
				{
					Debug.Log ("Splitted message " + i + " = '" + messages[i] + "'");
					if (messages[i] != "" && messages[i][0] == '{')
						gameController.inputMessagesBuffer.Add (messages[i]);
				}
			}
			
		}
		
		Debug.Log ("Input stream closed...");
		
	}

	private IEnumerator processAsyncOutputSocketConnection() {
		
		Debug.Log ("Output stream started...");
		
		while (isConnectionAlive)
		{
			List<string> messagesToSend = new List<string>();

			if (gameController.outputMessagesBuffer.Count != 0)
				lock (gameController.outputMessagesBuffer) {
					messagesToSend.AddRange(gameController.outputMessagesBuffer);
					gameController.outputMessagesBuffer.Clear();
				}

			foreach (string str in messagesToSend)
			{
				Debug.Log ("Sending message: " + str);

				byte[] buf = System.Text.Encoding.UTF8.GetBytes(str);
				//socket.Send(buf);
				/*yield return */StartCoroutine(coSocket.Send(cosocketCallback, buf));
			}
			yield return null;
		}
		
		Debug.Log ("Output stream closed...");
	}

	private object cosocketCallback(SocketAsyncEventArgs saea) {
		SocketError[] forbiddenStates = {
			SocketError.AccessDenied,
			SocketError.AddressAlreadyInUse,
			SocketError.ConnectionAborted,
			SocketError.ConnectionRefused,
			SocketError.ConnectionReset,
			SocketError.Disconnecting,
			SocketError.Fault,
			SocketError.HostDown,
			SocketError.HostNotFound,
			SocketError.HostUnreachable,
			SocketError.Interrupted,
			SocketError.InvalidArgument,
			SocketError.NetworkDown,
			SocketError.NetworkReset,
			SocketError.NetworkUnreachable,
			SocketError.NotConnected,
			SocketError.OperationAborted,
			SocketError.OperationNotSupported,
			SocketError.Shutdown,
			SocketError.SocketError,
			SocketError.TimedOut,
			SocketError.TooManyOpenSockets,
			SocketError.TryAgain
		};
	
		for (int i = 0; i < forbiddenStates.Length; i++)
			if (forbiddenStates[i] == saea.SocketError)
				{
					Debug.LogError ("Socket problem :" + saea.SocketError.ToString());
					Debug.LogError("Closing connections");
					
					Stop();
				}

		return null;
	}

	private static Socket ConnectSocket(string server, int port)
	{
		Socket s = null;
		IPHostEntry hostEntry = null;
		
		// Get host related information.
		hostEntry = Dns.GetHostEntry(server);
		
		// Loop through the AddressList to obtain the supported AddressFamily. This is to avoid
		// an exception that occurs when the host IP Address is not compatible with the address family
		// (typical in the IPv6 case).
		foreach(IPAddress address in hostEntry.AddressList)
		{
			print("Trying connect to " + address +":"+ port);
			
			IPEndPoint ipe = new IPEndPoint(address, port);
			Socket tempSocket = 
				new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);



			tempSocket.Connect(ipe);
			
			if(tempSocket.Connected)
			{
				print ("Connected");
				return tempSocket;
			}
			else
			{
				continue;
			}
		}
		return s;
	}
}
